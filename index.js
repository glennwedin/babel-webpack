
export class Parent {
	constructor(string) {
		this.text = string;
	}
	foo(a, b) {
		return a+b;
	}
}

export class Child extends Parent {
	constructor(string) {
		super(string);
	}

	bar(a) {
		return foo(a+2);
	}
}
