var webpack = require('webpack');

module.exports = {
	entry: "./index.js",
    output: {
        path: __dirname,
        filename: "./dist/out.js"
    },
    module: {
    	loaders: [
			{
	        	test: /\.js$/,
	        	loader: 'babel-loader'
	    	}
	    ]
	}
}
